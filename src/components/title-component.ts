import {html, css, LitElement} from 'lit';
import {customElement} from 'lit/decorators.js';

@customElement('title-component')
export class TitleComponent extends LitElement {
  static override styles = css``;

  override render() {
    return html` <h1>H1 salut</h1> `;
  }
}
